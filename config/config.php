<?php
session_start();

$db_url = getenv("DB_URL") !== "" ? getenv("DB_URL") : "mysql";
$db_user = getenv("DB_USER") !== "" ? getenv("DB_USER") : "user";
$db_pw = getenv("DB_PW") !== "" ? getenv("DB_PW") : "password";


$db = adoNewConnection('mysqli');
$db->debug = getenv("DEV") ? true : false;
$db->connect($db_url , $db_user, $db_pw, 'gallery');

include("../sql/init/app_user/appUserInit.php");

